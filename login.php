<?php
  if(isset($_GET["hash"])){
      $auth_data = $_GET;
      $check_hash = $auth_data['hash'];
      unset($auth_data['hash']);
      $data_check_arr = [];
      foreach ($auth_data as $key => $value) {
        $data_check_arr[] = $key . '=' . $value;
      }
      sort($data_check_arr);
      $data_check_string = implode("\n", $data_check_arr);
      $secret_key = hash('sha256', "geheim", true);
      $hash = hash_hmac('sha256', $data_check_string, $secret_key);
      if (strcmp($hash, $check_hash) !== 0) {
        echo('Data is NOT from Telegram');
      } else {
          session_start();
          $_SESSION["uid"] = $_GET["id"]; 
          header('Location: chat.php');
      }
  } else {
      echo '
      <body>
    <script async src="https://telegram.org/js/telegram-widget.js?5" data-telegram-login="nerdsclubbot" data-size="large" data-auth-url="https://oli-z.tk/tgnc/login.php" data-request-access="write"></script>
    </body>
      ';
  }

?>