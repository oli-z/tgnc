<?php
    require_once('main.inc.php');
    if (isset($_GET["chat"])) {
        $_SESSION["chat"] = $_GET["chat"];
        if (!isset ($_SESSION["uid"])) { header('Location: login.php'); die(); }
        header('Location: chat.php'); die();
    } else {
        echo '
            <form>
                <label for="chat">Chat id: </label>
                <input name="chat"></input>
                <button type="submit">send</button>
            </form>
            <h2>How to get chat id?</h2>
            <h3>Method 1 - Telegram Web</h3>
            <ol>
                <li>Open chat in TG Web</li>
                <li>Take this part of the URL: <br/><img src="https://i.imgur.com/NjiNBUC.png"></img></li>
                <li>Add "-100" in front of it</li>
            </ol>
            
            <h3>Method 2 - Plus Messenger</h3>
            <ol>
                <li>Find chat id in Plus Messenger</li>
                <li>Add "-100" as a prefix</li>
            </ol>
            
            <h3>Method 3 - Help menu</h3>
            <ol>
                <li>enter "ncbot help" in a group where the bot is a member</li>
                <li>click on the link for the web interface</li>
            </ol>
        ';
    };
?>

