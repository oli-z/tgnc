<?php
    session_start();
    require "TgApi.php";
    require "libchart/libchart/classes/libchart.php";
    $config = parse_ini_file("config.ini", TRUE);
    $TgApi = new TgApi($config["tg"]["apikey"]);
    $db = new PDO('mysql:host='.$config["db"]["host"].';dbname='.$config["db"]["name"].';charset=utf8mb4', $config["db"]["user"], $config["db"]["password"]);
    $gd_apikey = $config["gd"]["apikey"];         // API-Key Google Drive (Quelle für Inline-Bot)
    $gd_folderid = $config["gd"]["folderid"]; 
    $tg_apikey = $config["tg"]["apikey"];
?>