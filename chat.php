<?php
    require("main.inc.php");
    if(!$TgApi->isUserInGroup($_SESSION["uid"], $_SESSION["chat"])) die("User is not in group. If you are member of the group but the bot still doesn't see you please go to the bots profile on telegram and press \"start\".");
    
    $chat = $TgApi->getChatInfo($_SESSION["chat"]);
    
    if (isset($_GET["delabbr"])) {
        $sql = $db->prepare("DELETE FROM tgnc_abbreviations where aid = ?");
        $sql->execute([$_GET["delabbr"]]);
    }
    
    if (isset($_GET["privacy"])) {
        $sql = $db->prepare("DELETE FROM tgnc_words where user = ?");
        $sql->execute([$_SESSION["uid"]]);
        // $sql = $db->prepare("DELETE FROM tgnc_words where uid = ?");
        // $sql->execute([$_SESSION["uid"]]);
    }
    
    if (isset($_POST["abbreviation"]) && isset($_POST["explanation"])) {
        $sql = $db->prepare("INSERT INTO tgnc_abbreviations (abbreviation, explanation, chat_id) VALUES (?, ?, ?)");
        $sql->execute([$_POST["abbreviation"], $_POST["explanation"], $_SESSION["chat"]]);
    }
    
    echo "
    <head>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" crossorigin=\"anonymous\"></script>
    </head>
    <body>
    ";
    
    echo '
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4 fixed-top">
      <a class="navbar-brand" href="#">NC Bot</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link" href="#stats">Statistics</a></li>
          <li class="nav-item"><a class="nav-link" href="#abbreviations">Abbreviations</a></li>
          <li class="nav-item"><a class="nav-link" href="#privacybutton">Privacy button</a></li>
        </ul>
            <ul class="nav navbar-nav navbar-right" style="color: #fff;">
                <li style="padding-top: 0.5em; margin-right: 1em;">Chat: <b>'.$chat["title"].'</b></li>
                <li><a class="btn btn-primary" href="logout.php">Logout</a></li>
            </ul>
      </div>
    </nav>
    ';
    
    echo "<div class='container jumbotron' style='margin-top: 6em;'>";
    
    echo "<h2 id=\"stats\">Statistics</h2>";
    
    echo "<h3>Most used words</h3>";
    
    $sql = $db->prepare("SELECT word, SUM(count) as countsum FROM tgnc_words WHERE chat_id = ? GROUP BY word ORDER BY countsum DESC LIMIT 15");
    $sql->execute([$_SESSION["chat"]]);
    $words = $sql->fetchAll(PDO::FETCH_ASSOC);
    
    	$dataSet = new XYDataSet();
    foreach($words as $word) {
        $dataSet->addPoint(new Point($word["word"], $word["countsum"]));
    }
    $chart = new PieChart(768, 500);
    $chart->setDataSet($dataSet);
    $chart->setTitle("Most used words in chat");
    $fn = uniqid();
    $chart->render($fn);
    $img_b64 = base64_encode(file_get_contents($fn));
    unlink($fn);
    echo "<image src='data:image/png;base64,".$img_b64."'></image>";
    echo "<br/><br/><br/><br/>";
    
    // SELECT tgnc_words.user, tgnc_users.firstname, tgnc_users.lastname, tgnc_users.username, SUM(count) as countsum FROM tgnc_words LEFT JOIN tgnc_users ON tgnc_words.user = tgnc_users.uid WHERE chat_id = -1001140096031 AND word = "ich" GROUP BY user ORDER BY countsum DESC LIMIT 10
    
    echo "<h3>Most used words are most used by:</h3>";
    
    foreach ($words as $word) {
        echo "<h4>".$word["word"]."</h4>";
            $sql = $db->prepare("SELECT tgnc_words.user, tgnc_users.firstname, tgnc_users.lastname, tgnc_users.username, SUM(count) as countsum FROM tgnc_words LEFT JOIN tgnc_users ON tgnc_words.user = tgnc_users.uid WHERE chat_id = ? AND word = ? GROUP BY user ORDER BY countsum DESC");
            $sql->execute([$_SESSION["chat"], $word["word"]]);
            $wordusers = $sql->fetchAll(PDO::FETCH_ASSOC);
            
            $dataSet = new XYDataSet();
            foreach($wordusers as $worduser) {
                $dataSet->addPoint(new Point($worduser["user"]. " - ".$worduser["username"] . " - " .$worduser["firstname"]." ".$worduser["lastname"], $worduser["countsum"]));
            }
            $chart = new PieChart(768, 500);
            $chart->setDataSet($dataSet);
            $chart->setTitle("Word \"".$word["word"]."\" is most used by:");
            $fn = uniqid();
            $chart->render($fn);
            $img_b64 = base64_encode(file_get_contents($fn));
            unlink($fn);
            echo "<image src='data:image/png;base64,".$img_b64."'></image>";
            echo "<br/><br/><br/><br/>";
    }
    

    
    
    
    
    $sql = $db->prepare("SELECT * FROM tgnc_abbreviations WHERE chat_id = ?");
    $sql->execute([$_SESSION["chat"]]);
    $abbreviations = $sql->fetchAll();
    echo "<br/><br/><br/><br/>";
    echo "<h2 id=\"abbreviations\">Abbreviations</h2>";
    echo("<div class=\"card\" style=\"padding: 1em; margin-bottom: 1em;\">
            <form method='post'>
                <div class=\"input-group mb-3\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"abbreviation\">Abbreviation:</span>
                    </div>
                    <input required type=\"text\" class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"abbreviation\" name=\"abbreviation\">
                </div>
                
                <div class=\"input-group mb-3\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"explanation\">Explanation:</span>
                    </div>
                    <input required type=\"text\" class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"explanation\" name=\"explanation\">
                </div>
                
                
                <button type='submit' class='btn'>Add</button></form></div>");
    
    echo("<table class=\"table table-hover\"><thead><tr><td><b>Abbreviation</b></td><td><b>Explanation</b></td><td>Options</td></tr></thead><tbody>");
    foreach($abbreviations as $abbreviation) {
        echo "<tr><td>";
        print_r($abbreviation["abbreviation"]);
        echo "</td><td>";
        print_r($abbreviation["explanation"]);
        echo "</td><td>";
        echo "<a href='?delabbr=".$abbreviation["aid"]."' class='btn btn-danger'>delete</a>";
        echo "</td></tr>";
    }
    echo("</tbody></table>");

    echo("<h2 id=\"privacybutton\">Privacy button</h2><a href=\"?privacy\" class=\"btn btn-danger btn-lg\">Delete every information the bot knows about me (This can not be undone!)</a>");
    echo "</div>";
    echo "</body>";
?>