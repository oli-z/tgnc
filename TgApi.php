<?php

class TgApi {
    private $apikey;
    
    function __construct ($apikey) {
        $this->apikey = $apikey;
        $result = json_decode(file_get_contents("https://api.telegram.org/bot". $this->apikey ."/getMe"), true);
        if($result["ok"] != 1) throw new Exception("Error initializing Telegram API");
    }
    
    function isUserInGroup($user, $group) {
        global $TG_APIKEY;
        $result = json_decode(file_get_contents("https://api.telegram.org/bot". $this->apikey ."/getChatMember?user_id=".$user."&chat_id=".$group), true);
        if ($result["ok"]) return true; else return false;
    }
    
    function userInGroupInfo($user, $group) {
        global $TG_APIKEY;
        $result = json_decode(file_get_contents("https://api.telegram.org/bot". $this->apikey ."/getChatMember?user_id=".$user."&chat_id=".$group), true);
        if ($result["ok"]) return $result["result"]; else return false;
    }
    
    function getChatInfo($chat) {
        global $TG_APIKEY;
        $result = json_decode(file_get_contents("https://api.telegram.org/bot". $this->apikey ."/getChat?chat_id=".$chat), true);
        if ($result["ok"]) return $result["result"]; else return false;
    }
    
    function sendMessage($chat_id, $text, $reply_to_message_id = null) {
        global $TG_APIKEY;
        $url = "https://api.telegram.org/bot". $this->apikey ."/sendMessage?chat_id=" . $chat_id . "&text=" . urlencode($text) . "&parse_mode=HTML";
        if (!is_null($reply_to_message_id)) $url .= "&reply_to_message_id=" . $reply_to_message_id;
        file_get_contents($url);
    }
}

?>