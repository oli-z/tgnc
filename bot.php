<?php
    $allowed_users = [                                                      // erlaubte User für Inline-Bot

    ];
    
    require_once("main.inc.php");
    
    // allgemeine Funktionen //
    function writeLog($log) {
        $fp = fopen('file.txt', 'a');
        fwrite($fp, print_r($log, TRUE));
        fwrite($fp, "--------------------------------------------\n");
        fclose($fp);
    }
    
    function startsWith($haystack, $needle) {
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
    }
    
    function endsWith($haystack, $needle) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }
    
    $rcv = file_get_contents('php://input');
    $r = json_decode($rcv, true);
    writeLog($r);
    
    if(isset($r["message"]["from"])){
        $sql = $db->prepare("INSERT INTO tgnc_users (uid, username, firstname, lastname, language_code) VALUES (:uid, :username, :firstname, :lastname, :language_code) ON DUPLICATE KEY UPDATE uid=:uid, username=:username, firstname=:firstname, lastname=:lastname, language_code=:language_code");
        $sql->bindValue(":uid", intval($r["message"]["from"]["id"]));
        if (isset($r["message"]["from"]["username"]))
            $sql->bindValue(":username", $r["message"]["from"]["username"]);
        else
            $sql->bindValue(":username", "");
        if (isset($r["message"]["from"]["first_name"]))
            $sql->bindValue(":firstname", $r["message"]["from"]["first_name"]);
        else
            $sql->bindValue(":firstname", "");
        if (isset($r["message"]["from"]["last_name"]))
            $sql->bindValue(":lastname", $r["message"]["from"]["last_name"]);
        else
            $sql->bindValue(":lastname", "");
        if (isset($r["message"]["from"]["language_code"]))
            $sql->bindValue(":language_code", $r["message"]["from"]["language_code"]);
        else
            $sql->bindValue(":language_code", "");
        $sql->execute();
        //$msg = print_r($r["message"]["from"], true);
    }
    
    if (isset($r["inline_query"])) {                    // Inline-Bot-Funktion
        $return_array["inline_query_id"] = $r["inline_query"]["id"];
        $return_array["cache_time"] = "0";
        writeLog($r);
        if(in_array($r["inline_query"]["from"]["id"], $allowed_users)){
        //if (1) {
            $gd = json_decode( file_get_contents("https://www.googleapis.com/drive/v2/files?q=%27" . $gd_folderid . "%27+in+parents&key=" . $gd_apikey), true );
            foreach($gd["items"] as $key => $item) {
                $mime_types = ["image/jpeg", "image/png"];
                if (in_array($item["mimeType"], $mime_types)){
                    $return_array_result["type"] = "photo";
                    $return_array_result["id"] = (string)$key;
                    $return_array_result["photo_url"] = $item["webContentLink"];
                    $return_array_result["thumb_url"] = $item["thumbnailLink"];
                    $return_array_result["title"] = $item["title"];
                    $return_array_result["caption"] = $item["title"];
                    $return_array_result["description"] = $item["title"];
                    $return_array_results[] = $return_array_result;
                }
            }
        } else {
            $return_array_results[0]["type"] = "photo";
            $return_array_results[0]["id"] = "0";
            $return_array_results[0]["photo_url"] = "https://oli-z.tk/tg-il-nc/denied.jpg";
            $return_array_results[0]["thumb_url"] = "https://oli-z.tk/tg-il-nc/denied.jpg";
        }
        $return_array["results"] = json_encode($return_array_results);
        $qs = http_build_query($return_array);
        // todo: move to TgApi class
        file_get_contents("https://api.telegram.org/bot". $tg_apikey ."/answerInlineQuery?".$qs);
    } elseif (isset($r["message"]["chat"]["id"])) {
        ///// BOT COMMANDS /////
        if (startsWith($r["message"]["text"], "ncbot")) {
            if (startsWith($r["message"]["text"], "ncbot add abbr")) {
                $regex = "/(?<=ncbot add abbr ).+/";
                preg_match($regex, $r["message"]["text"], $cmd);
                $cmd = explode("=", $cmd[0]);
                $stmt = $db->prepare("INSERT INTO tgnc_abbreviations (abbreviation, explanation, chat_id) VALUES (?, ?, ?)");
                $stmt->execute([$cmd[0], $cmd[1], $r["message"]["chat"]["id"]]);
                if ($stmt) $msg = "Erfolgreich in Datenbank eingetragen."; else $msg = "Es gab wohl einen Fehler.";
            } elseif (startsWith($r["message"]["text"], "ncbot rem abbr")) {
                $regex = "/(?<=ncbot rem abbr ).+/";
                preg_match($regex, $r["message"]["text"], $cmd);
                //$cmd = explode("=", $cmd[0]);
                $stmt = $db->prepare("DELETE FROM tgnc_abbreviations WHERE abbreviation = ? AND chat_id = ?");
                $stmt->execute([$cmd[0], $r["message"]["chat"]["id"]]);
                if ($stmt) $msg = "Erfolgreich aus Datenkbank entfernt."; else $msg = "Es gab wohl einen Fehler.";
            
            } elseif (startsWith($r["message"]["text"], "ncbot topwords")) {
                $stmt = $db->prepare("SELECT word, SUM(count) as countsum FROM tgnc_words WHERE chat_id = ? GROUP BY word ORDER BY countsum DESC LIMIT 5");
                $stmt->execute([ $r["message"]["chat"]["id"] ]); $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $msg = "<b>Das sind die Top 5 Wörter im Chat seit dem Hinzufügen des Bots: </b>\n";
                foreach ($result as $i=>$word) {
                    $msg .= ($i+1) . " - " . $word["word"]. " (" . $word["countsum"] . " mal gesagt)" ."\n";
                }
            } elseif (startsWith($r["message"]["text"], "ncbot help")) {
    $help = 
"<b>[[[ NerdsClub Bot - Hilfe ]]]</b>
    
    <b>Abkürzungen</b>
        Abkürzung hinzufügen:
            <i>ncbot add abbr [Abkürzung]=[Erklärung]</i>
        Abkürzung entfernen:
            <i>ncbot rem abbr [Abkürzung]</i>
    
    <b>Weitere Funktionen</b>
        Top-Wörter im Chat abfragen:
            <i>ncbot topwords</i>
        Bot-Webinterface:
            <a href='https://oli-z.tk/tgnc/?chat=".$r["message"]["chat"]["id"]."'>aufrufen</a>";
                $msg = $help;
            } else {
                $msg = "Dieser Befehl wurde nicht erkannt. Für eine Hilfe gib <i>ncbot help</i> ein.";
            }
            
            
            
        } else {
            $regex = "/(?<=\s|^)[\w\d-]+(?=\s|,|\.|$)/m";
            preg_match_all($regex, $r["message"]["text"], $words);
            $words = $words[0];
            foreach($words as $i=>$word) {
                
                ///// WORD STATISTICS STUFF /////
                $stmt= $db->prepare("SELECT * FROM tgnc_words WHERE word = :word AND user = :user AND chat_id = :chat_id");
                $stmt->execute([ "word" => $word, "user" => $r["message"]["from"]["id"], "chat_id" => $r["message"]["chat"]["id"] ]); 
                if ($stmt->fetch())
                    $stmt = $db->prepare("UPDATE tgnc_words SET count = count + 1 WHERE word = :word AND user = :user AND chat_id = :chat_id");
                else
                    $stmt = $db->prepare("INSERT INTO tgnc_words (user, word, count, chat_id) VALUES (:user, :word, 1, :chat_id)");
                 $stmt->execute([ "word" => $word, "user" => $r["message"]["from"]["id"], "chat_id" => $r["message"]["chat"]["id"] ]);
                 
                ///// ABBREVIATION STUFF /////
                $stmt = $db->prepare("SELECT * FROM tgnc_abbreviations WHERE abbreviation = ? AND chat_id = ?");
                $stmt->execute(array($word, $r["message"]["chat"]["id"]));
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($result) {
                    if ($i != 0) $msg .= " ";
                    $msg .= "<b>" . ucfirst($result["abbreviation"]). "</b> heißt übrigens <b>" . $result["explanation"] . "</b>.";
                }
                
                
            }
        }
        # if ($msg) file_get_contents("https://api.telegram.org/bot". TG_APIKEY ."/sendMessage?chat_id=" . $r["message"]["chat"]["id"] . "&text=" . urlencode($msg) . "&reply_to_message_id=" . $r["message"]["message_id"] . "&parse_mode=HTML");
        if ($msg) $TgApi->sendMessage($r["message"]["chat"]["id"], $msg, $r["message"]["message_id"]);
        
    }
?>