<?php
    session_start();
    unset($_SESSION);
    setcookie(session_name(), '', 100);
	session_unset();
	session_destroy();
    $_SESSION = array();
    header("Location: index.php");
?>